package com.example.employeemanager.service;

import com.example.employeemanager.model.User;
import com.example.employeemanager.repository.UserRepository;
import com.example.employeemanager.response.ResponseEp;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public ResponseEp saveUser(User user) {
        ResponseEp res = null;

        try {
            String checkEmail = user.getEmail();

            if (checkEmail != null && !"".equals(checkEmail)) {
                User userObj = fetchEmailById(checkEmail);

                if(userObj != null) {
                    res = new ResponseEp();
                    res.setMessageString("User already exists");
                    res.setStatusCode(409);
                    res.setUserId(userObj.getId());
                }
                else {
                    userRepository.save(user);
                    res = new ResponseEp();
                    res.setMessageString("User save successfully");
                    res.setStatusCode(200);
                    res.setUserId(1);
                }
            }
        } catch (Exception exception) {
            res = new ResponseEp();
            res.setMessageString("Error in user saving");
            res.setStatusCode(405);
            res.setUserId(0);
        }
        return res;
    }


    public ResponseEp successUser(User user) {
        ResponseEp res = null;

        try {
            String checkMail = user.getEmail();
            String checkPassword = user.getPassword();

            User userObj = null;
            if (checkMail != null && checkPassword != null) {
                userObj = fetchEmailAndPassword(checkMail, checkPassword);
            }
            if (userObj == null) {
                res = new ResponseEp();
                res.setMessageString("Invalid User");
                res.setStatusCode(401);
            }
            else {
                res = new ResponseEp();
                res.setMessageString("Login Successful");
                res.setStatusCode(200);
                res.setUserId(1);
            }
        }
        catch (Exception exception) {
            res = new ResponseEp();
            res.setMessageString("Error in login");
            res.setStatusCode(405);
            res.setUserId(0);
        }
        return res;
    }


    public User fetchEmailById(String email) {
        return userRepository.findByEmail(email);
    }

    public User fetchEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email,password);
    }

}
