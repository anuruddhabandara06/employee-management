package com.example.employeemanager;

import com.example.employeemanager.model.User;
import com.example.employeemanager.response.ResponseEp;
import com.example.employeemanager.service.UserService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/userRegister")
    public ResponseEp registrationUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @PostMapping("/userLogin")
    public ResponseEp loginUser(@RequestBody User user) {
        return userService.successUser(user);
    }
}
